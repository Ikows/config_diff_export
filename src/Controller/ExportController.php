<?php

namespace Drupal\config_diff_export\Controller;

use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExportController.
 */
class ExportController extends ControllerBase {

  /**
   * Drupal\Core\Config\StorageInterface definition.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorageSync;

  /**
   * Drupal\Core\Config\CachedStorage definition.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected $configStorage;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Config\StorageInterface $sync_storage
   *   The source storage.
   * @param \Drupal\Core\Config\StorageInterface $active_storage
   *   The target storage.
   */
  public function __construct(StorageInterface $sync_storage, StorageInterface $active_storage) {
    $this->syncStorage = $sync_storage;
    $this->activeStorage = $active_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.storage.sync'),
      $container->get('config.storage'),
    );
  }


  /**
   * Export-cu.
   *
   * @return string
   *   Return Hello string.
   */
  public function exportCU() {

    $storage_comparer = new StorageComparer($this->syncStorage, $this->activeStorage);
    $changeList = $storage_comparer->createChangelist()->getChangelist();
    $cUArray = array_merge($changeList["create"], $changeList["update"]);
    $tar = new ArchiveTar(file_directory_temp() . '/diff-export.tar');

    foreach ($cUArray as $configName) {
      $tar->addString($configName . '.yml', $filename, Yaml::encode($this->activeStorage->read($configName)));
    }

    return new BinaryFileResponse(file_directory_temp() . '/diff-export.tar', 200, [
      'Content-type' => 'application/tar',
      'Content-Disposition' => 'attachment; filename="diff-export.tar"',
    ]);
  }

}
